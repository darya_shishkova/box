package ru.darya.demobox;

import java.util.Scanner;

/**
 * <b>Класс, демонстрирующий работу с объектами.</b>
 */
public class Main {
    static Scanner scanner = new Scanner(System.in);
    private static final String YOUR_FORMAT = "Объем вашей коробки %s равен %.2f %n";
    private static final String MY_FORMAT = "Объем моей коробки %s равен %.2f %n";

    public static void main(String[] args) {
        double w, h, f;
        String boxInfo;
        do {
            System.out.println("Введите длину, ширину и высоту коробки через пробел");
            boxInfo = scanner.nextLine().replace(',', '.');
            String arguments[] = boxInfo.split(" ", 3);
            w = Double.valueOf(arguments[0]);
            h = Double.valueOf(arguments[1]);
            f = Double.valueOf(arguments[2]);
            if (w <= 0 || h <= 0 || f <= 0) {
                System.out.println("Кажется, вы допустили ошибку, попробуйте ввести данные еще раз!");
            }
        } while (w <= 0 || h <= 0 || f <= 0);
        Box box = new Box(w, h, f); //Создаём первую коробку
        double boxVolume = box.boxCapacity();
        System.out.printf(YOUR_FORMAT, box, boxVolume);
        Box myBox = new Box(10, 20, 5); //Создаём вторую
        double myBoxVolume = myBox.boxCapacity();
        System.out.printf(MY_FORMAT, myBox, myBoxVolume);
        if (boxVolume > myBoxVolume) {
            System.out.println("Объём Вашей коробки больше моей :(");
        } else if (boxVolume < myBoxVolume) {
            System.out.println("Завидуйте :D Моя коробка больше Вашей!");
        } else {
            System.out.println("Наши коробки равны по объёму :)");
        }
    }

}