package ru.darya.demobox;

public class Box {
    /**
     * Ширина
     */
    private double width;

    /**
     * Высота
     */
    private double height;

    /**
     * Глубина
     */
    private double depth;

    Box(double width, double height, double depth) {
        this.width = width;
        this.height = height;
        this.depth = depth;
    }


    Box(double side) {
        this(side, side, side);
    }

    Box() {
        this(1);
    }


    @Override
    public String toString() {
        return "{" +
                "width=" + width +
                ", height=" + height +
                ", depth=" + depth +
                '}';
    }

    double boxCapacity() {
        return width * height * depth;
    }
}